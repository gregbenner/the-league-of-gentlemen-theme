<?php
/*
Template Name: Contact Us
*/
?>

<?php get_header(); ?>
			
			<div id="content" class="contact-us">
			
				<div id="inner-content" class="wrap clearfix">
			
				    <div id="main" class="clearfix" role="main">

					   <h1><?php the_title(); ?></h1>

						<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
					
					    <article id="post-<?php the_ID(); ?>" <?php post_class('clearfix'); ?> role="article">
						
						   	<div class="grid_4 first">
								<?php the_content(); ?>
							</div>

						    <section class="grid_8 last"> 
							   <iframe width="645" height="371" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://maps.google.co.za/maps?hl=en&amp;q=the+league+of+gentlemen&amp;ie=UTF8&amp;hq=the+league+of+gentlemen&amp;hnear=Cape+Town,+Western+Cape&amp;ll=-34.088848,18.843184&amp;spn=0.038313,0.070467&amp;t=m&amp;z=14&amp;iwloc=A&amp;cid=13873745394258178109&amp;output=embed"></iframe>
						    </section> <!-- end article section -->
						
	
					
					    </article> <!-- end article -->
					
					    <?php endwhile; ?>	
					
					    <?php else : ?>
					
					    <?php endif; ?>
			
				    </div> <!-- end #main -->
    
				    <?php // get_sidebar(); ?>
				    
				</div> <!-- end #inner-content -->
    
			</div> <!-- end #content -->

<?php get_footer(); ?>
