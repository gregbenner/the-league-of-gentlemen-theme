<?php
/*
Template Name: Our Services
*/
?>

<?php get_header(); ?>

<div id="content" class="services">
			
				<div id="inner-content" class="wrap clearfix">
				<div id="main" class=" first clearfix" role="main">

<?php
	$args = array(
	'post_type' => 'services',
	'post_status' => 'publish',
	'posts_per_page' => -1,
	'orderby' => 'menu_order',
	'order' => 'ASC' 
);
$posts = new WP_Query( $args ); ?>

<div class="fl clearfix">
	<h2><?php the_title(); ?></h2>
</div>
 
<article class="fl">

<ul>
<?php if ($posts -> have_posts()) : while ($posts -> have_posts()) : $posts -> the_post(); ?>
	<li class="fl">
		<div class="grid_3 first">
		<div class="theServiceTitle">
			<h3><a href="<?php echo get_permalink(); ?>"><?php the_title(); ?></a></h3>
		</div>
			<a class="serviceImageHolder" href="<?php echo get_permalink(); ?>"><?php the_post_thumbnail("bones-services-thumb" ,array( 'class'	=> "imageBorder")); ?></a>
		</div>

		<div class="grid_6">
			<?php // the_content(); ?>
			<?php 
			 $content = get_the_content();
		     $content = strip_tags($content);
		     echo substr($content, 0, 492). ' ...';
			?>
			<p>
				<a href="<?php the_permalink(); ?>" class="more">More</a>
			</p>
		</div>

		<div class="grid_3 requestAquoteDiv last">
			<a class="requestAquoteButton" href="<?php echo home_url(); ?>/get-a-quote/"><img src="<?php echo get_template_directory_uri(); ?>/library/images/UI/Request-a-Quote.png"  /></a>
		</div>



<span class="hr"></span>

</li>

<?php endwhile; else : endif; ?>
<?php wp_reset_postdata(); ?>

</ul>


<div class="servicesBottomMargin">
	<span class="backToTop"><a href="#top"></a></span>
</div>

</article> <!-- end article -->



</div> <!-- end #inner-content -->
    
</div> <!-- end #content -->

<?php get_footer(); ?>

		