<?php
/*
Template Name: Slider
*/
?>

<?php get_header(); ?>
			
			<div id="content">
			
				<div id="inner-content" class="wrap clearfix">
			
				<div id="main" class=" first clearfix" role="main">

		    	<?php
		    	$args = array(
				'post_type' => 'slide',
				'post_status' => 'publish',
				'posts_per_page' => -1
				);
		    	$args2 = array(
				'post_type' => 'attachment',
				'post_mime_type' => 'image',
				'post_parent' => $post->ID
				);
				$images = get_posts( $args2 );

				$posts = new WP_Query( $args ); ?>

				<?php if ($posts -> have_posts()) : while ($posts -> have_posts()) : $posts -> the_post(); ?>
						

			    <article id="post-<?php the_ID(); ?>" <?php post_class('clearfix'); ?> role="article">
				

					<section>
							    

					    <div class="leftSide">
					    	<?php the_post_thumbnail("full"); ?> 
						</div>

						<div class="rightSide">
							<?php the_content(); ?>
						</div>

					 </section> <!-- end article section -->

				
				  </article> <!-- end article -->
					
					    <?php endwhile; else : endif; ?>

					    <?php wp_reset_postdata(); ?>
			
				    </div> <!-- end #main -->

				    <div class="featuredServices">

							<h3>Featured Services</h3>

							<div class="first grid_3">
								<h4>7 Day golf tour</h4>
								The golf courses selected are in or near Cape Town and are of championship  standard-with...
								<a href="#" class="more">More</a>
							</div>

							<div class="grid_3">
								<h4>Full day tours</h4>
								Consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua...
								<a href="#" class="more">More</a>
							</div>

							<div class="grid_3">
								<h4>Weddings and celebrations</h4>
								We pride ourselves in having the most refined wedding and celebration transport offering...
								<a href="#" class="more">More</a>
							</div>

							<div class="last grid_3">
								<h4>Car rental</h4>
								We are proudly accredited agents for the well known and respected internationalcar rental company... Sixt.
								<a href="#" class="more">More</a>
							</div>

						</div>

					<?php wp_reset_query(); ?>


					<section class="entry-content clearfix" itemprop="articleBody">
					<?php $id="home"; $post = get_page($id); $content = apply_filters('the_content', $post->post_content); echo $content;  ?>
					</section> <!-- end article section -->


				

				<?php // endwhile; //else : endif; ?>
	    
				</div> <!-- end #inner-content -->
    
			</div> <!-- end #content -->

<?php get_footer(); ?>
