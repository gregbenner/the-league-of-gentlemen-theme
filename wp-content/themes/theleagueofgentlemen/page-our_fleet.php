<?php
/*
Template Name: Our Fleet
*/
	$bigCounter = 1;
	get_header(); 
?>

<div id="content">
			
	<div id="inner-content" class="wrap clearfix">
	<div id="main" class=" first clearfix" role="main">

<?php
	$args = array(
	'post_type' => array('fleet', 'people-movers'),
	'post_status' => 'publish',
	'posts_per_page' => -1
);
$posts = new WP_Query( $args ); ?>

<div class="fl clearfix">
	<h2 class="ourfleetheading">Our Fleet </h2> <h4 class="britishcollectablesheading"> Entire Fleet</h4>
</div>

<article class="fl">

<ul>
<?php if ($posts -> have_posts()) : while ($posts -> have_posts()) : $posts -> the_post(); ?>
	<li class="fl clearfix aCar">
		<div class="titleHolder">
		<span class="carTitle"><?php the_title(); ?> |</span> <?php $my_post_meta = get_post_meta($post->ID, 'car_make', true); 
		switch ($my_post_meta) {
		 case "Bentley": ?>
		  	 <img class="BentleyIMG" src='<?php echo get_template_directory_uri(); ?>/library/images/logos/bentley.svg' alt="Bentley | Logo" height="24" />
		 <?php 
		 break;
		 case "Daimler": ?>
		  	<img class="DaimlerIMG" src='<?php echo get_template_directory_uri(); ?>/library/images/logos/daimler.svg' alt="Daimler | Logo" height="24" />
		 <?php
		 break;
		 case "Jaguar" : ?>
		 	 <img class="JaguarIMG" src='<?php echo get_template_directory_uri(); ?>/library/images/logos/jaguar.svg' alt="Jaguar | Logo" height="24" />
		 <?php
		 break;
		 case "MG" : ?>
		 	 <img class="MGIMG" src='<?php echo get_template_directory_uri(); ?>/library/images/logos/mg.svg' alt="MG | Logo" height="24" />
		 <?php
		 break;
		 case "Rolls Royce" : ?>
		 	 <img class="RollsRoyceIMG" src='<?php echo get_template_directory_uri(); ?>/library/images/logos/RR.svg' alt="Rolls Royce | Logo" height="24" />
		 <?php
		 break;

		 case "SsangYong" : ?>
		 	 <img class="SsangYongIMG" src='<?php echo get_template_directory_uri(); ?>/library/images/logos/ssang.svg' alt="Rolls Royce | Logo" height="24" />
		 <?php
		 break;

		 case "Toyota" : ?>
		 	 <img class="ToyotaIMG" src='<?php echo get_template_directory_uri(); ?>/library/images/logos/toyota.svg' alt="Rolls Royce | Logo" height="24" />
		 <?php
		 break;

		 case "Chrysler" : ?>
		 	 <img class="ChryslerIMG" src='<?php echo get_template_directory_uri(); ?>/library/images/logos/chrysler.svg' alt="Rolls Royce | Logo" height="24" />
		 <?php
		 break;
		 default:
		 #default
		 } ?>
		</div>

	<div class="grid_4 first">
		

<ul class="carStats">

<?php $engine_size = get_post_meta($post->ID, 'engine_size', true);
if ($engine_size) {  ?>
<li>Engine Size: <span class="amountStat"><?php print_custom_field('engine_size'); ?></span></li>
<?php } ?>

<?php $cylinders = get_post_meta($post->ID, 'cylinders', true);
if ($cylinders) {   ?>
<li>Cylinders: <span class="amountStat"><?php print_custom_field('cylinders'); ?></span></li>
<?php } ?>

<?php $power_output = get_post_meta($post->ID, 'power_output', true);
if ($power_output) {   ?>
<li>Power Output: <span class="amountStat"><?php print_custom_field('power_output'); ?></span></li>
<?php } ?>

<?php $engine_size = get_post_meta($post->ID, 'engine_size', true);
if ($engine_size) {   ?>
<?php } ?>

<?php $engine_size = get_post_meta($post->ID, 'engine_size', true);
if ($engine_size) {   ?>
<?php } ?>

<?php $engine_size = get_post_meta($post->ID, 'engine_size', true);
if ($engine_size) {   ?>
<?php } ?>

<?php $my_post_meta = get_post_meta($post->ID, 'rear_seats', true); if (!empty($my_post_meta)) { ?>		
<li>Rear Seats: <span class="amountStat"><?php print_custom_field('rear_seats'); } ?></span></li>

<?php $capacity = get_post_meta($post->ID, 'capacity', true);
if ($capacity) {   ?>
<li>Capacity: <span class="amountStat"><?php print_custom_field('capacity'); ?></span></li>
<?php } ?>

<?php $luggage_space = get_post_meta($post->ID, 'luggage_space', true);
if ($luggage_space) {   ?>
<li>Luggage Space: <span class="amountStat"><?php print_custom_field('luggage_space'); ?></span></li>
<?php } ?>

</ul>

	<h5 class="historyTitle">History</h5>
		<?php the_content(); ?>
	</div>
	

		
<div class="grid_8 last">
	<div class="holder galleries">

		<?php $imageCounter = 1; ?>
		
		<ul class="myGallery<?php echo $imageCounter; ?>">
		
		<?php
					$args = array(
					'order'          => 'ASC',
					'orderby'        => 'menu_order',
					'post_type'      => 'attachment',
					'post_parent'    => $post->ID,
					'post_mime_type' => 'image',
					'post_status'    => null,
					'numberposts'    => -1,
					);
					$attachments = get_posts($args);
					if ($attachments) {
						foreach ($attachments as $attachment) {
		?>
		<li>
			<img src="<?php echo wp_get_attachment_url($attachment->ID); ?>" width="620" height="357" class="mainGalleryImage" />
		</li>
		
		<?php  } } $imageCounter++; ?>

	</div>
</div>

<span class="backToTop"><a href="#top"></a></span>
<span class="hr"></span>

</li>
<?php endwhile; else : endif; ?>
<?php wp_reset_postdata(); ?>

</ul>



</article> <!-- end article -->



</div> <!-- end #inner-content -->
    
</div> <!-- end #content -->

<?php get_footer(); ?>

		