<?php
/*
Template Name: Full Day Tours
*/
?>

<?php get_header(); ?>

<div id="content" class="services fulldaytours">
			
				<div id="inner-content" class="wrap clearfix">
			
				<div id="main" class=" first clearfix" role="main">

<?php
	$counter = 0;
	$args = array(
	'post_type' => 'services',
	'category_name' => 'full-day-tours',
	'post_status' => 'publish',
	'order' 	=> 'ASC',
	'orderby' => 'menu_order',
	'posts_per_page' => -1
);

$first_one = 0;
$posts = new WP_Query( $args ); ?>

<div class="fl servicesMainTitle clearfix">
	<h1>Services</h1>
	<h3><?php the_title(); ?></h3>
</div>
 
<article class="fl">

<ul>
<?php if ($posts -> have_posts()) : while ($posts -> have_posts()) : $posts -> the_post(); ?>
	<li class="fl serviceRow row" data-counter=<?php echo $counter; ?> >
		<div class="grid_9 first">
		
		<div class="row">
			<div class="theServiceTitle">
				<img src="<?php print_custom_field('title_icon:to_image_src'); ?>" class="titleIcon" />
				<h3 id="<?php the_title(); ?>"><a href="<?php echo get_permalink(); ?>"><?php the_title(); ?></a></h3>
			</div>
		</div>
		<div class="row">

			<div style="width: 40%; float: left;">
				<a class="serviceImageHolder" href="<?php echo get_permalink(); ?>"><?php the_post_thumbnail("full" ,array( 'class'	=> "imageBorder")); ?></a>
			</div>
	
			<p style="width: 60%; float: left; margin: 0;">
			 <?php 
			 $content = get_the_content();
		     $content = strip_tags($content);
		     echo substr($content, 0, 492) . ' ...';
		     ?>
		     </p>
		     <p style="float: left;">
		    	 <a href="" class="more">More</a>
		 	</p>
		     <br>
		     <a class="requestAquoteButton" href="<?php echo home_url(); ?>/get-a-quote/">
				<img src="<?php echo home_url(); ?>/wp-content/themes/theleagueofgentlemen/library/images/UI/Request-a-Quote.png">
			</a>
			

		

		</div>

		</div>

		

		<?php if ($first_one < 1) { ?>
		<div class="grid_3 widgetySidebar last">
			<ul>
				<li class="row">
					<a class="label" href="#Wine Tour"><span class="icon" id="wine-tour"></span>Wine Tour</a>
				</li>
				<li class="row">
					<a class="label" href="#Peninsula Tour"><span class="icon" id="peninsula-tour"></span>Peninsula Tour</a>
				</li>
				<li class="row">
					<a class="label" href="#Whale Watching Tour"><span class="icon" id="whale-watching-tour"></span>Whale Watching Tour</a>
				</li>
				<li class="row">
					<a class="label" href="#Cape Town City Tour"><span class="icon" id="cape-town-city-tour"></span>Cape Town City Tour</a>
				</li>
				<li class="row">
					<a class="label" href="#Hermanus Tour"><span class="icon" id="hermanus-tour"></span>Hermanus Tour</a>
				</li>
				<li class="row">
					<a class="label" href="#L’ Agulhas Tour"><span class="icon" id="lagulhas-tour"></span>L’Agulhas Tour</a>
				</li>
			</ul>
		</div>
		<?php } $first_one++; ?>

		<span class="hr"></span>
</li>


<?php $counter++; endwhile; else : endif; ?>
<?php wp_reset_postdata(); ?>

</ul>


<div class="servicesBottomMargin">
	<span class="backToTop"><a href="#top"></a></span>
</div>

</article> <!-- end article -->



</div> <!-- end #inner-content -->
    
</div> <!-- end #content -->

<?php get_footer(); ?>

		