<?php
/*
Template Name: Page Home
*/
?>

<?php get_header(); ?>
			
			<div id="content">
			
				<div id="inner-content" class="wrap clearfix">
			

		    	<?php
		    	$args = array(
				'post_type' => 'slide',
				'post_status' => 'publish',
				'posts_per_page' => -1
				);
		    	$args2 = array(
				'post_type' => 'attachment',
				'post_mime_type' => 'image',
				'post_parent' => $post->ID
				);
				$images = get_posts( $args2 );

				$posts = new WP_Query( $args ); ?>

				
			<div id="banner-fade">
			    <ul class="bjqs">

				<?php if ($posts -> have_posts()) : while ($posts -> have_posts()) : $posts -> the_post(); ?>
					
				<li>
					
							    

					    <div class="leftSide">
					    	<?php the_post_thumbnail("full"); ?> 
						</div>

						<div class="rightSide">
							<?php the_content(); ?>
							<img src="wp-content/uploads/membercapetowntoursim.png" alt="membercapetowntoursim" width="44" height="42" class="membercapetowntoursim" />
						</div>

						<!-- <div class="grid_9 first">
					    	<?php // the_post_thumbnail("full"); ?> 
						</div>

						<div class="grid_3 last">
							<?php // the_content(); ?>
							<img src="wp-content/uploads/membercapetowntoursim.png" alt="membercapetowntoursim" width="44" height="42" class="membercapetowntoursim" />
						</div> -->

					

				</li>
				
				 
					
					    <?php endwhile; else : endif; ?>
					    <?php wp_reset_postdata(); ?>

					
			</ul>
		</div>
				    

				    <div class="featuredServices">

							<h3>Featured Services</h3>

							<div class="first one_fourth">
								<?php  if ( dynamic_sidebar('homewidget_one') ) : else : endif; ?>
								<span class="homePageDivider"></span>
							</div>


							<div class="one_fourth">
								<?php  if ( dynamic_sidebar('homewidget_two') ) : else : endif; ?>
								<span class="homePageDivider"></span>
							</div>

							<div class="one_fourth">
								<?php  if ( dynamic_sidebar('homewidget_three') ) : else : endif; ?>
								<span class="homePageDivider"></span>
							</div>

							<div class="last one_fourth">
								<?php  if ( dynamic_sidebar('homewidget_four') ) : else : endif; ?>
							</div>

						</div>

					</div> <!-- end #inner-content -->
    
					</div> <!-- end #content -->

					<?php wp_reset_query(); ?>

			

					<section class="grid_9 first" itemprop="articleBody">					
					<?php 
					$id="home"; $post = get_page_by_title('home'); $content = apply_filters('the_content', $post->post_content); echo $content;  
					?>
					<span class="whiteBorder"></span>
					</section> <!-- end article section -->

					<?php
					$args = array(
					'post_type' => 'news',
					'post_status' => 'publish',
					'posts_per_page' => 2
					);
					$posts = new WP_Query( $args );
					?>
					<div class="grid_3 last clearfix newsFeed">
						<h1 class="white">News</h1>
						<?php if ($posts -> have_posts()) : while ($posts -> have_posts()) : $posts -> the_post(); ?>
						<h4 class=""><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>
						<?php 
						 $content = get_the_content();
					     $content = strip_tags($content);
					     echo substr($content, 0, 50);
						 ?>
						 <br>
						<a href="<?php the_permalink(); ?>">Read more</a>
						<?php endwhile; else : endif; ?>
					    <?php wp_reset_postdata(); ?>
					</div>

<?php get_footer(); ?>
