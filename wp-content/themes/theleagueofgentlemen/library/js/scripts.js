/*
Bones Scripts File
Author: Eddie Machado

*/

jQuery.noConflict();
                 
jQuery(function ($) {

//Full day tours page, adjust paragraph position
  // jQuery("li[data-counter]").each(function() {
  //   topVal = jQuery(this).find(".serviceImageHolder").position().top;
  //   jQuery(this).find("p").css({
  //     'top' : topVal,
  //     'position' : 'absolute'
  //   });
  // });

// Another fricken gallery plugin since the we need the next and previous arrows do not regular functions

var howManyCarsInTheHouse = jQuery(".aCar").length;

if ( jQuery('.galleries').length ) {
    for (var i=1; i < howManyCarsInTheHouse + 1; i++) {
      jQuery('.myGallery' +  i).galleryView({
        'panel_width' : 600,
        'panel_height' : 370,
        'frame_width' : 110,
        'frame_height' : 80,
        'frame_gap' : 12,
        'panel_scale' : 'scale'
        //'panel_height' : 'auto'
      });
    }
     
}


// Modal all links with simple modal class
            jQuery('a.simpleModal').click(function(){
              jQuery(this).find('img').modal();
                return false;
            });

// galleryies // Not good enough pparently!
  // if ($(".cr-container").length) {
  //   jQuery(".cr-container").crotator();
  // }

           
// Home page slider
            if($("#banner-fade").length) {
            jQuery('#banner-fade').bjqs({
                width : "100%",
                responsive : false,
                animspeed : 5500, // the delay between each slide
                automatic : true // automatic
              });
            }

        svgeezy.init('nocheck', 'png');

// Smooth Scroll

    function filterPath(string) {
      return string
        .replace(/^\//,'')
        .replace(/(index|default).[a-zA-Z]{3,4}$/,'')
        .replace(/\/$/,'');
      }
      var locationPath = filterPath(location.pathname);
      var scrollElem = scrollableElement('html', 'body');
     
      $('a[href*=#]').each(function() {
        var thisPath = filterPath(this.pathname) || locationPath;
        if (  locationPath == thisPath
        && (location.hostname == this.hostname || !this.hostname)
        && this.hash.replace(/#/,'') ) {
          var $target = $(this.hash), target = this.hash;
          if (target) {
            var targetOffset = $target.offset().top;
            $(this).click(function(event) {
              event.preventDefault();
              $(scrollElem).animate({scrollTop: targetOffset}, 400, function() {
                location.hash = target;
              });
            });
          }
        }
      });
 
      // use the first element that is "scrollable"
      function scrollableElement(els) {
        for (var i = 0, argLength = arguments.length; i <argLength; i++) {
          var el = arguments[i],
              $scrollElement = $(el);
          if ($scrollElement.scrollTop()> 0) {
            return el;
          } else {
            $scrollElement.scrollTop(1);
            var isScrollable = $scrollElement.scrollTop()> 0;
            $scrollElement.scrollTop(0);
            if (isScrollable) {
              return el;
            }
          }
        }
        return [];
      }

      // add down state to main nav

      function addActiveState() {
        // $(".navigation li").mouseup(function(){
        //   $(this).removeClass('active');
        // }).mousedown(function(){
        //   $(this).addClass('active');
        // });
        $(".navigation #menu-main > li").bind({
          click: function(){
            $(this).addClass("active");
          },
          mousedown: function() {
            $(this).addClass("active");
          },
          mouseup: function(){
            $(this).removeClass("active");
          },
          mouseleave: function(){
            $(this).removeClass("active");
          }
        });

        $(".navigation #menu-main > li .sub-menu li").bind({
          click: function(){
            $(this).addClass("sub-active");
          },
          mousedown: function() {
            $(this).addClass("sub-active");
          },
          mouseup: function(){
            $(this).removeClass("sub-active");
          },
          mouseleave: function(){
            $(this).removeClass("sub-active");
          }
        });
    
      }
      //addActiveState();


}); // close dom ready


// IE8 ployfill for GetComputed Style (for Responsive Script below)
if (!window.getComputedStyle) {
    window.getComputedStyle = function(el, pseudo) {
        this.el = el;
        pseudo = pseudo || undefined;
        this.getPropertyValue = function(prop) {
            var re = /(\-([a-z]){1})/g;
            if (prop === 'float') { prop = 'styleFloat'; }
            if (re.test(prop)) {
                prop = prop.replace(re, function () {
                    return arguments[2].toUpperCase();
                });
            }
            return el.currentStyle[prop] ? el.currentStyle[prop] : null;
        };
        return this;
    };
}


/*! A fix for the iOS orientationchange zoom bug.
 Script by @scottjehl, rebound by @wilto.
 MIT License.
*/
(function(w){
	// This fix addresses an iOS bug, so return early if the UA claims it's something else.
	if( !( /iPhone|iPad|iPod/.test( navigator.platform ) && navigator.userAgent.indexOf( "AppleWebKit" ) > -1 ) ){ return; }
    var doc = w.document;
    if( !doc.querySelector ){ return; }
    var meta = doc.querySelector( "meta[name=viewport]" ),
        initialContent = meta && meta.getAttribute( "content" ),
        disabledZoom = initialContent + ",maximum-scale=1",
        enabledZoom = initialContent + ",maximum-scale=10",
        enabled = true,
		x, y, z, aig;
    if( !meta ){ return; }
    function restoreZoom(){
        meta.setAttribute( "content", enabledZoom );
        enabled = true; }
    function disableZoom(){
        meta.setAttribute( "content", disabledZoom );
        enabled = false; }
    function checkTilt( e ){
		aig = e.accelerationIncludingGravity;
		x = Math.abs( aig.x );
		y = Math.abs( aig.y );
		z = Math.abs( aig.z );
		// If portrait orientation and in one of the danger zones
        if( !w.orientation && ( x > 7 || ( ( z > 6 && y < 8 || z < 8 && y > 6 ) && x > 5 ) ) ){
			if( enabled ){ disableZoom(); } }
		else if( !enabled ){ restoreZoom(); } }
	w.addEventListener( "orientationchange", restoreZoom, false );
	w.addEventListener( "devicemotion", checkTilt, false );
})( this );