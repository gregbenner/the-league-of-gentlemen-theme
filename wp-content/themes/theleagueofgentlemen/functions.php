<?php
/************* INCLUDE NEEDED FILES ***************/

require_once('library/bones.php'); // if you remove this, bones will break
require_once('library/admin.php'); // this comes turned off by default

/************* THUMBNAIL SIZE OPTIONS *************/

// Thumbnail sizes
add_image_size( 'bones-thumb-600', 600, 150, true );
add_image_size( 'bones-thumb-300', 300, 100, true );

add_image_size( 'bones-gallery-thumb', 209, 125, true );
add_image_size( 'bones-contact-645', 645, 371, true );
add_image_size( 'bones-fleet-647', 647, 372, true );
add_image_size( 'bones-services-thumb', 211, 162, true );

add_image_size( 'bones-services-single-thumb', 191, 183, true );

/* 
and change the dimensions & name. As long as you
to add more sizes, simply copy a line from above 
upload a "featured image" as large as the biggest
set width or height, all the other sizes will be
auto-cropped.

You can change the names and dimensions to whatever
you like. Enjoy!
*/

/************* ACTIVE SIDEBARS ********************/

// Sidebars & Widgetizes Areas
function bones_register_sidebars() {

    register_sidebar(array(
    	'id' => 'sidebar1',
    	'name' => __('Sidebar 1', 'bonestheme'),
    	'description' => __('The first (primary) sidebar.', 'bonestheme'),
    	'before_widget' => '<div id="%1$s" class="widget %2$s">',
    	'after_widget' => '</div>',
    	'before_title' => '<h4 class="widgettitle">',
    	'after_title' => '</h4>',
    ));
    

    register_sidebar(array(
        'id' => 'address',
        'name' => __('Address'),
        'description' => __('the address found in footer', 'bonestheme'),
        'before_widget' => '<div id="%1$s" class="widget %2$s">',
        'after_widget' => '</div>',
        'before_title' => '<p class="address">',
        'after_title' => '</p>',
    ));
    register_sidebar(array(
    	'id' => 'homewidget_one',
    	'name' => __('Home Widget One'),
    	'description' => __('first column home page', 'bonestheme'),
    	'before_widget' => '<div id="%1$s" class="widget %2$s">',
    	'after_widget' => '</div>',
    	'before_title' => '<h4 class="widgettitle">',
    	'after_title' => '</h4>',
    ));
    register_sidebar(array(
        'id' => 'homewidget_two',
        'name' => __('Home Widget Two'),
        'description' => __('The second column homepage', 'bonestheme'),
        'before_widget' => '<div id="%1$s" class="widget %2$s">',
        'after_widget' => '</div>',
        'before_title' => '<h4 class="widgettitle">',
        'after_title' => '</h4>',
    ));
    register_sidebar(array(
        'id' => 'homewidget_three',
        'name' => __('home Widget Three'),
        'description' => __('The third column homepage.', 'bonestheme'),
        'before_widget' => '<div id="%1$s" class="widget %2$s">',
        'after_widget' => '</div>',
        'before_title' => '<h4 class="widgettitle">',
        'after_title' => '</h4>',
    ));
    register_sidebar(array(
        'id' => 'homewidget_four',
        'name' => __('home Widget Four'),
        'description' => __('The fourth column home page.', 'bonestheme'),
        'before_widget' => '<div id="%1$s" class="widget %2$s">',
        'after_widget' => '</div>',
        'before_title' => '<h4 class="widgettitle">',
        'after_title' => '</h4>',
    ));
    
    
} // don't remove this bracket!



/************* SEARCH FORM LAYOUT *****************/

// Search Form
function bones_wpsearch($form) {
    $form = '<form role="search" method="get" id="searchform" action="' . home_url( '/' ) . '" >
    <label class="screen-reader-text" for="s">' . __('Search for:', 'bonestheme') . '</label>
    <input type="text" value="' . get_search_query() . '" name="s" id="s" placeholder="'.esc_attr__('Search the Site...','bonestheme').'" />
    <input type="submit" id="searchsubmit" value="'. esc_attr__('Search') .'" />
    </form>';
    return $form;
} // don't remove this bracket!

//Google Maps Shortcode
function fn_googleMaps($atts, $content = null) {
   extract(shortcode_atts(array(
      "width" => '640',
      "height" => '480',
      "src" => ''
   ), $atts));
   return '<iframe class="bordered" width="'.$width.'" height="'.$height.'" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="'.$src.'&amp;output=embed"></iframe>';
}
add_shortcode("googlemap", "fn_googleMaps");

function sc_postimage($atts, $content = null) {
    extract(shortcode_atts(array(
        "size" => 'thumbnail',
        "float" => 'none'
    ), $atts));
    $images =& get_children( 'post_type=attachment&post_mime_type=image&post_parent=' . get_the_id() );
    foreach( $images as $imageID => $imagePost )
    $fullimage = wp_get_attachment_image($imageID, $size, false);
    $imagedata = wp_get_attachment_image_src($imageID, $size, false);
    $width = ($imagedata[1]+2);
    $height = ($imagedata[2]+2);
    return '<div class="postimage" style="width: '.$width.'px; height: '.$height.'px; float: '.$float.';">'.$fullimage.'</div>';
}
add_shortcode("postimage", "sc_postimage");



/**
* Define a constant path to our single template folder
*/
define(SINGLE_PATH, TEMPLATEPATH . '/single');

/**
* Filter the single_template with our custom function
*/
add_filter('single_template', 'my_single_template');

/**
* Single template function which will choose our template
*/
function my_single_template($single) {
    global $wp_query, $post;

/**
* Checks for single template by ID
*/
if(file_exists(SINGLE_PATH . '/single-' . $post->ID . '.php'))
    return SINGLE_PATH . '/single-' . $post->ID . '.php';
    
/**
* Checks for single template by category
* Check by category slug and ID
*/
foreach((array)get_the_category() as $cat) :

    if(file_exists(SINGLE_PATH . '/single-cat-' . $cat->slug . '.php'))
        return SINGLE_PATH . '/single-cat-' . $cat->slug . '.php';

    elseif(file_exists(SINGLE_PATH . '/single-cat-' . $cat->term_id . '.php'))
        return SINGLE_PATH . '/single-cat-' . $cat->term_id . '.php';

endforeach;

/**
* Checks for single template by tag
* Check by tag slug and ID
*/

$wp_query->in_the_loop = true;

foreach((array)get_the_tags() as $tag) :

    if(file_exists(SINGLE_PATH . '/single-tag-' . $tag->slug . '.php'))
        return SINGLE_PATH . '/single-tag-' . $tag->slug . '.php';

    elseif(file_exists(SINGLE_PATH . '/single-tag-' . $tag->term_id . '.php'))
        return SINGLE_PATH . '/single-tag-' . $tag->term_id . '.php';

endforeach;

$wp_query->in_the_loop = false;
/**
* Checks for single template by author
* Check by user nicename and ID
*/

$curauth = get_userdata($wp_query->post->post_author);

    if(file_exists(SINGLE_PATH . '/single-author-' . $curauth->user_nicename . '.php'))
        return SINGLE_PATH . '/single-author-' . $curauth->user_nicename . '.php';

    elseif(file_exists(SINGLE_PATH . '/single-author-' . $curauth->ID . '.php'))
        return SINGLE_PATH  . '/single-author-' . $curauth->ID . '.php';
/**
* Checks for default single post files within the single folder
*/
    if(file_exists(SINGLE_PATH . '/single.php'))
        return SINGLE_PATH . '/single.php';

    elseif(file_exists(SINGLE_PATH . '/default.php'))
        return SINGLE_PATH . '/default.php';

return $single;

}

?>
