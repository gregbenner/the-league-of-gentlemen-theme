<?php
/*
Template Name: Get a Quote
*/
?>

<?php get_header(); ?>
			
			<div id="content" class="get-a-quote">
			
				<div id="inner-content" class="wrap clearfix">
			
				    <div id="main" class="first clearfix" role="main">

					   

						<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
					
					    <article id="post-<?php the_ID(); ?>" <?php post_class('clearfix'); ?> role="article">
						
						   	<div class="grid_4 first">
								<?php the_content(); ?>
							</div>

						    <section class="grid_8 last">
						    	<?php the_post_thumbnail("bones-contact-645" ,array( 'class'	=> "imageBorder")); ?>
							   
						    </section> <!-- end article section -->
						
	
					
					    </article> <!-- end article -->
					
					    <?php endwhile; ?>	
					
					    <?php else : ?>
					
					    <?php endif; ?>
			
				    </div> <!-- end #main -->
    
				    <?php // get_sidebar(); ?>
				    
				</div> <!-- end #inner-content -->
    
			</div> <!-- end #content -->

<?php get_footer(); ?>
