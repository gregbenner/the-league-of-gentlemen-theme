<?php
/*
Template Name: Single Services
*/
?>

<?php get_header(); ?>

<div id="content" class="services">
			
				<div id="inner-content" class="wrap clearfix">
			
				<div id="main" class=" first clearfix" role="main">



<div class="fl clearfix">
	<h2 class="servicesTitle">Services</h2>
</div>
 
<article class="fl">

<ul>
<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

	<li class="fl">
		<div class="grid_9 first">
		<div class="row">
			<div class="theServiceTitle">
				<h3 class="serviceTitle"><?php the_title(); ?></h3> 
				<?php $subheading = get_post_meta($post->ID, 'subheading', true); if ($subheading) {  ?> 
				<span class="subheading"> | <?php print_custom_field('subheading'); ?></span>
				<?php } ?>
				<!-- Insert Tagline -->
			</div>
		</div>

		<div class="row">
			<?php the_content(); ?>
		</div>

		

		


<?php $monday = get_post_meta($post->ID, 'monday', true); if ($monday) {  ?>
		<div class="row">
			<div class="grid_3 first">
				<img src="<?php print_custom_field('mondays_picture:to_image_src'); ?>"  class="bordered" />
			</div>

			<div class="grid_6 daysHolder last clearfix">
			<h3>Monday</h3>
			<span class="hr"></span>
			<?php print_custom_field('monday'); ?>
			</div>
		</div>
<?php } ?>

<?php $tuesday = get_post_meta($post->ID, 'tuesday', true); if ($tuesday) {  ?>
		<div class="row">
			<div class="grid_3 first">
				<img src="<?php print_custom_field('tuesdays_image:to_image_src'); ?>" class="bordered" />
			</div>

			<div class="grid_6 daysHolder last clearfix">
				<h3>Tuesday</h3>
				<span class="hr"></span>
				<?php print_custom_field('tuesday'); ?>
			</div>
		</div>
<?php } ?>

<?php $wednesday = get_post_meta($post->ID, 'wednesday', true); if ($wednesday) {  ?>
		<div class="row">
			<div class="grid_3 first">
				<img src="<?php print_custom_field('wednesdays_image:to_image_src'); ?>" class="bordered" />
			</div>

			<div class="grid_6 daysHolder last clearfix">
			<h3>Wednesday</h3>
			<span class="hr"></span>
			<?php print_custom_field('wednesday'); ?>
			</div>
		</div>
<?php } ?>

<?php $thursday = get_post_meta($post->ID, 'thursday', true); if ($thursday) {  ?>
		<div class="row">
			<div class="grid_3 first">
				<img src="<?php print_custom_field('thursdays_image:to_image_src'); ?>" class="bordered" />
			</div>

			<div class="grid_6 daysHolder last clearfix">
			<h3>Thursday</h3>
			<span class="hr"></span>
				<?php print_custom_field('thursday'); ?>
			</div>
		</div>
<?php } ?>

<?php $friday = get_post_meta($post->ID, 'friday', true); if ($friday) {  ?>
		<div class="row">
			<div class="grid_3 first">
				<img src="<?php print_custom_field('fridays_image:to_image_src'); ?>" class="bordered" />
			</div>

			<div class="grid_6 daysHolder last clearfix">
			<h3>Friday</h3>
			<span class="hr"></span>
				<?php print_custom_field('friday'); ?>
			</div>
		</div>
<?php } ?>

<?php $saturday = get_post_meta($post->ID, 'saturday', true); if ($saturday) {  ?>
		<div class="row">
			<div class="grid_3 first">
				<img src="<?php print_custom_field('saturdays_image:to_image_src'); ?>" class="bordered" />
			</div>

			<div class="grid_6 daysHolder last clearfix">
			<h3>Saturday</h3>
			<span class="hr"></span>
				<?php print_custom_field('saturday'); ?>
			</div>
		</div>
<?php } ?>

</div>

<div class="grid_3 sidebarWidget last clearfix">
			
			<?php $days_event = get_post_meta($post->ID, 'days_event', true); if ($days_event) {  ?>
			<div class="row">
				<img src="<?php echo get_template_directory_uri(); ?>/library/images/UI/clock.svg" width="23" height="22" style="float: left;" /> 
				<h5 class="dayTour"><?php print_custom_field('days_event'); ?> Day Tour</h5>
			</div>
			<?php } ?>

			<?php $departs = get_post_meta($post->ID, 'departs', true); if ($departs) {  ?>
			<div class="row">
				Departs: <?php print_custom_field('departs'); ?>
			</div>
			<?php } ?>

			<a class="requestAquoteButton" href="<?php echo home_url(); ?>/get-a-quote/">
				<img src="<?php echo get_template_directory_uri(); ?>/library/images/UI/Request-a-Quote.png">
			</a>
			<div class="row">
			<?php the_post_thumbnail("bones-services-single-thumb" ,array( 'class'	=> "imageBorder")); ?>
			</div>
			
			<p> <?php print_custom_field('sidebar_description'); ?> </p>
</div>

<div class="row">
	<div class="first last grid_9">
		<h2 class="closing_notes"><?php print_custom_field('closing_notes'); ?></h2>
	</div>
</div>

		

		
</li>

<?php endwhile; else : endif; ?>
<?php wp_reset_postdata(); ?>

</ul>


<div class="servicesBottomMargin">
	<span class="backToTop"><a href="#top"></a></span>
</div>

</article> <!-- end article -->



</div> <!-- end #inner-content -->
    
</div> <!-- end #content -->

<?php get_footer(); ?>

		