<!doctype html>  

<!--[if lt IE 7]><html <?php language_attributes(); ?> class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if (IE 7)&!(IEMobile)]><html <?php language_attributes(); ?> class="no-js lt-ie9 lt-ie8"><![endif]-->
<!--[if (IE 8)&!(IEMobile)]><html <?php language_attributes(); ?> class="no-js lt-ie9"><![endif]-->
<!--[if gt IE 8]><!--> <html <?php language_attributes(); ?> class="no-js"><!--<![endif]-->
	
	<head>
		<meta charset="utf-8">
		
		<title><?php wp_title(''); ?></title>
		
		<!-- Google Chrome Frame for IE -->
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		
		<!-- mobile meta (hooray!) -->
		<meta name="HandheldFriendly" content="True">
		<meta name="MobileOptimized" content="320">
		<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
		
		<!-- icons & favicons (for more: http://themble.com/support/adding-icons-favicons/) -->
		<link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/favicon.png">

		<link href='http://fonts.googleapis.com/css?family=PT+Sans:400,700' rel='stylesheet' type='text/css'>
				
  		<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">
		
		<!-- wordpress head functions -->
		<?php wp_head(); ?>
		<!-- end of wordpress head -->
			
		<!-- drop Google Analytics Here -->
		<!-- end analytics -->
		
	</head>
	
	<body <?php body_class(); ?>>
		<span id="top"></span>
	
		<div id="container" class="container_12">
			
			<header class="header" role="banner">
			
				<div id="inner-header" class="clearfix">

					<?php if ( is_page("home") ) { ?>
					<a href="<?php echo home_url(); ?>" rel="nofollow"><img id="logo" src="<?php echo get_template_directory_uri(); ?>/library/images/logo.png" /></a>
					<?php }  else { ?>
					<div class="behindTheHeader">
						<div class="logoHolder pagesNav">
							<span class="overTheHeader"></span>
							<a href="<?php echo home_url(); ?>" rel="nofollow"><img id="logo" src="<?php echo get_template_directory_uri(); ?>/library/images/logo-noGrad.png" /></a>
						</div>
					</div>
					<?php } ?>

					
					
					
					
					<nav role="navigation" class="navigation">
						<?php bones_main_nav(); ?>
						<div class="connectFacebook">
							<a target="_blank" href="https://www.facebook.com/pages/The-League-of-Gentlemen-Transport/507503885940314?fref=ts">Connect with us on Facebook 
								<!-- <img src="<?php echo get_template_directory_uri(); ?>/library/images/icons/facebook.png" alt="facebook" /> -->
							
								<div class="facebookIcon" alt='facebook'></div>
							</a>
						</div>
					</nav>

					
				
				</div> <!-- end #inner-header -->
			
			</header> <!-- end header -->
