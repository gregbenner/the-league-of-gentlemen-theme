<?php
/*
Template Name: Gallery
*/
?>

<?php get_header(); ?>
			
			<div id="content">
			
				<div id="inner-content" class="wrap clearfix">
			
				    <div id="main" class="first clearfix" role="main">

					   <?php
							$args = array(
							'post_type' => 'gallery',
							'post_status' => 'publish',
							'posts_per_page' => -1
						);
						$posts = new WP_Query( $args ); ?>

						<header class="page-header">
							
							  <h1 class="page-title"><?php the_title(); ?></h1>

						</header>

						<ul class="theGallery">

						<?php if ($posts -> have_posts()) : while ($posts -> have_posts()) : $posts -> the_post(); ?>
						
						<li class="item">
							<a href="<?php $feat_image = wp_get_attachment_url( get_post_thumbnail_id($post->ID) );
echo $feat_image; ?>" class="lightbox" rel="lightbox[gallery]"><span></span>
							<?php the_post_thumbnail("bones-gallery-thumb"); ?>
							</a>
						</li>

					
					    <?php endwhile;?>	
						
						</ul>

					    <?php else : endif;?>
			
				    </div> <!-- end #main -->
    
				  
				    
				</div> <!-- end #inner-content -->
    
			</div> <!-- end #content -->

<?php get_footer(); ?>
