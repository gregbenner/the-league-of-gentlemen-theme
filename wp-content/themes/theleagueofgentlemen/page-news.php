<?php
/*
Template Name: News
*/
?>

<?php get_header(); ?>


			
			<div id="content">
			
				<div id="inner-content" class="wrap clearfix">
			
				    <div id="main" class="first clearfix" role="main">

					   <?php
							$args = array(
							'post_type' => 'news',
							'post_status' => 'publish',
							'posts_per_page' => -1
						);
						$posts = new WP_Query( $args ); ?>

						<?php if ($posts -> have_posts()) : while ($posts -> have_posts()) : $posts -> the_post(); ?>
					
					    <article id="post-<?php the_ID(); ?>" <?php post_class('clearfix'); ?> role="article">
						
						    <header class="article-header">
							
							  <h1 class="page-title"><?php the_title(); ?></h1>

			                  <p class="byline vcard"><?php
			                    printf(__('Posted <time class="updated" datetime="%1$s" pubdate>%2$s</time>.', 'bonestheme'), get_the_time('Y-m-j'), get_the_time(__('F, jS, Y', 'bonestheme')), bones_get_the_author_posts_link());
			                  ?></p>

						
						    </header> <!-- end article header -->
					
						    <section class="entry-content">
							    <?php the_content(); ?>
						    </section> <!-- end article section -->
						
						    <footer class="article-footer">
							    <p class="clearfix"><?php the_tags('<span class="tags">' . __('Tags:', 'bonestheme') . '</span> ', ', ', ''); ?></p>
							
						    </footer> <!-- end  footer -->
						    
						    <?php // comments_template(); ?>
					
					    </article> <!-- end article -->
					
					    <?php endwhile; ?>	
					
					    <?php else : ?>
					
        					
					
					    <?php endif; ?>
			
				    </div> <!-- end #main -->
    
				    <?php // get_sidebar(); ?>
				    
				</div> <!-- end #inner-content -->
    
			</div> <!-- end #content -->

<?php get_footer(); ?>
