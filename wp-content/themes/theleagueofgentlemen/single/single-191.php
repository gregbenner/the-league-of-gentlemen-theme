<?php get_header(); ?>

<div id="content" class="services">
			
				<div id="inner-content" class="wrap clearfix">
			
				<div id="main" class=" first clearfix" role="main">



<div class="fl clearfix">
	<h2 class="servicesTitle">Services</h2>
</div>
 
<article class="fl">

<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

		<div class="first" style="width: 451px; float: left;">
		<div class="row">
			<div class="theServiceTitle">
				<h3 class="serviceTitle"><?php the_title(); ?></h3> 
				<?php $subheading = get_post_meta($post->ID, 'subheading', true); if ($subheading) {  ?> 
				<span class="subheading"> | <?php print_custom_field('subheading'); ?></span>
				<?php } ?>
				<!-- Insert Tagline -->
			</div>
		</div>

		<div class="row">
			<?php the_content(); ?>
		</div>

</div>

<div class="last clearfix" style="width: 481px; float: left; margin-left: 25px;">
	<img src="<?php echo get_template_directory_uri(); ?>/library/images/airport/airport-transfer.jpg" />	
</div>


		



<?php endwhile; else : endif; ?>
<?php wp_reset_postdata(); ?>



</article> <!-- end article -->



</div> <!-- end #inner-content -->
    
</div> <!-- end #content -->

<?php get_footer(); ?>

