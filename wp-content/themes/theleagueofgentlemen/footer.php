		 </div> <!-- end #container -->

		</div>


			
			<footer class="footer" role="contentinfo">
			
				<div id="inner-footer" class="clearfix">
					
					<div class="contact">
						<div class="innerContact">

						<ul>
							<li><span>Tel </span> | 021 854 4827</li>
							<li><span>Cell </span> | 083 635 6410</li> 
							<li><span>E-mail </span> | <a href="mailto:info@theleagueofgentlemen.co.za" target="_blank">info@theleagueofgentlemen.co.za</a></li>
						</ul>
						
						<?php if ( dynamic_sidebar('address') ) : else : endif; ?> 
						<!-- <p class="address">Garvin Crescent, Gants Centre, Somerset-West  |  P.O. Box 3, Somerset-West, 7130</p>
					 -->

						<div class="logos">
							<ul>
								<li><img src="<?php echo get_template_directory_uri(); ?>/library/images/logos/daimler.svg" height="21" id="daimler" /></li>
								<li><img src="<?php echo get_template_directory_uri(); ?>/library/images/logos/bentley.svg" height="19" id="bentley" /></li>
								<li><img src="<?php echo get_template_directory_uri(); ?>/library/images/logos/RR.svg" height="36" id="RR" /></li>
								<li><img src="<?php echo get_template_directory_uri(); ?>/library/images/logos/jaguar.svg" height="23" id="jaguar" /></li>
							</ul>
						</div>

						</div>
					</div>

					

				</div> <!-- end #inner-footer -->

				<p class="source-org copyright">Copyright of The League of Gentlemen 2013</p>
				
			</footer> <!-- end footer -->
		
		
		
		<!-- all js scripts are loaded in library/bones.php -->
		<?php wp_footer(); ?>

	</body>

</html> <!-- end page. what a ride! -->
