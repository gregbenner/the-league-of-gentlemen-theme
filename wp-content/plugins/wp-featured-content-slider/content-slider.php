<?php

$c_slider_direct_path =  get_bloginfo('wpurl')."/wp-content/plugins/wp-featured-content-slider";

$c_slider_class = c_slider_get_dynamic_class();

?>

<script type="text/javascript">
	jQuery('#featured_slider ul').cycle({ 
		fx: '<?php $c_slider_effect = get_option('effect'); if(!empty($c_slider_effect)) {echo $c_slider_effect;} else {echo "scrollLeft";}?>',
		prev: '.feat_prev',
		next: '.feat_next',
		speed:  800, 
		timeout: <?php $c_slider_timeout = get_option('timeout'); if(!empty($c_slider_timeout)) {echo $c_slider_timeout;} else {echo 4000;}?>, 
		pager:  null
	});
</script>

<style>


.<?php echo $c_slider_class;?> {
font-size: 10px;
float: right;
clear: both;
position: relative;
top: -10px;
background-color: #<?php $c_slider_border = get_option('feat_border'); if(!empty($c_slider_border)) {echo $c_slider_border;} else {echo "CCC";}?>;
padding: 3px 3px;
line-height: 10px !important;
}

</style>

<div id="featured_slider">
	

	<ul id="slider">

		<?php
		
		$c_slider_sort = get_option('sort'); if(empty($c_slider_sort)){$c_slider_sort = "post_date";}
		$c_slider_order = get_option('order'); if(empty($c_slider_order)){$c_slider_order = "DESC";}
		$c_slider_limit = get_option('limit'); if(empty($c_slider_limit)){$c_slider_limit = 350;}
		$c_slider_points = get_option('points'); if(empty($c_slider_points)){$c_slider_points = "...";}
		$c_slider_post_limit = get_option('limit_posts'); if(empty($c_slider_limit_posts)){$c_slider_limit_posts = "-1";}
                
		global $wpdb;
	
		global $post;
		
		$args = array( 'meta_key' => 'feat_slider', 'meta_value'=> '1', 'suppress_filters' => 0, 'post_type' => array('post', 'page'), 'orderby' => $c_slider_sort, 'order' => $c_slider_order, 'numberposts'=> $c_slider_post_limit);
		
		$myposts = get_posts( $args );
		
		foreach( $myposts as $post ) :	setup_postdata($post);
			
			$c_slider_custom = get_post_custom($post->ID);
			
			$c_slider_thumb = c_slider_get_thumb("feat_slider");
			
		?>
		
		<li>
		<div class="img_left"><a href="<?php the_permalink();?>">
			<img src="<?php echo $c_slider_thumb;?>" /></a>
		</div>
		<div class="content_right">
			<h2><a href="<?php the_permalink();?>"><?php the_title();?></a></h2>
			<?php echo get_the_content();?>
		</div>

		</li>
		
		<?php endforeach; ?>
	
	</ul>
	
	<div class="feat_next"></div>
	<div class="feat_prev"></div>
	
</div>